﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace mi9.Tests
{
    [TestClass]
    public class Movie
    {
        [TestMethod]
        public void PostMovie_Test()
        {
            //Arrange

            var webClient = new WebClient();
            var requestJson = File.ReadAllText(@".\" + "MovieRequest.json");
            var responseJson = File.ReadAllText(@".\" + "MovieResponse.json");

            string data = webClient.UploadString("http://localhost/api/show", requestJson);

            Assert.AreEqual(data, responseJson);
        }

       
        internal static bool JsonEquals( string json, string otherjson)
        {
            var nJson = JObject.Parse(json).ToString();
            var nOtherJson = JObject.Parse(otherjson).ToString();
            return nJson == nOtherJson;
        }
    }
}
