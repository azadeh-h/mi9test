﻿using System.Collections.Generic;

namespace mi9.Shows
{
    public class TVShow
    {
        public string Country { get; set; }
        public string Description { get; set; }
        public bool DRM { get; set; }
        public int EpisodeCount { get; set; }
        public string Genre { get; set; }
        public TvShowImage Image { get; set; }
        public string Language { get; set; }
        public Episode NextEpisode { get; set; }
        public string PrimaryColour { get; set; }
        public List<KeyValuePair<string, string>> Seasons { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string TVChannel { get; set; }
    }
}