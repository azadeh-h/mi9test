﻿using Newtonsoft.Json;

namespace mi9.Shows
{
    public class ResponseData
    {
        [JsonProperty("image")]
        public string ShowImage { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }
        
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}