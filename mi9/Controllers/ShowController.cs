﻿using mi9.Shows;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace mi9.Controllers
{
    public class ShowController : ApiController
    {
        // POST api/values
        public HttpResponseMessage Post()
        {
            var response = new HttpResponseMessage();

            try
            {
                var request =
                    new StreamReader(HttpContext.Current.Request.InputStream).ReadToEnd();
                JObject movieSearch = JObject.Parse(request);

                // get JSON result objects into a list
                IList<JToken> results = movieSearch["payload"].Children().ToList();

                // serialize JSON results into .NET objects
                IList<TVShow> searchResults = results.Select(result => JsonConvert.DeserializeObject<TVShow>(result.ToString())).ToList();

                //filter the result and return the required fields only
                var filteredList =
                    searchResults.Where(m => m.DRM && m.EpisodeCount > 0)
                        .Select(m => new ResponseData  { ShowImage = m.Image.ShowImage,Slug = m.Slug, Title = m.Title })
                        .ToList();

                //serialize the result as a json string 
                string responseMovie = JsonConvert.SerializeObject(new { response = filteredList }, Formatting.Indented,
                    new JsonConverter[] { new StringEnumConverter() });

                //set the response contetnt to the json result
                response.Content = new StringContent(responseMovie);
            }
            catch (Exception)
            {
                //for any exception return badrequest

                response.StatusCode = HttpStatusCode.BadRequest;
                response.Content = new StringContent("{\"error\": \"Could not decode request: JSON parsing failed\"}");
            }
            //return the response. either filtered json objects or the error in json format
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return response;
        }
    }
}